public class VirtualPetApp{
	public static void main(String args[]){
		java.util.Scanner reader = new java.util.Scanner (System.in);
		String gender;
		boolean isGrassFed, liveOutdoor;
		Cattle[] laughingStock = new Cattle[4];
		
		for(int i=0; i<laughingStock.length; i++){
			//1st field
			System.out.println("This is the no." + (i+1) + " cattle. Type <male> or <female> to determine its gender.");
			gender = reader.next();
			
			//2nd field
			System.out.println("No." + (i+1) + " cattle. Type (1) if the cattle is grass-fed, (2) if it isn't");
			if (reader.nextInt() == 1)
				isGrassFed = true;
			else 
				isGrassFed = false;
			//3rd field
			System.out.println("No." + (i+1) + " cattle. Type (1) if the cattle lives outdoor, (2) if it does not");
			if (reader.nextInt() == 1)
				liveOutdoor = true;
			else 
				liveOutdoor = false;
			
			//create animal using constructor
			laughingStock[i] = new Cattle(gender, isGrassFed, liveOutdoor);
		}
		System.out.println(laughingStock[laughingStock.length-1].getGender() + " " + laughingStock[laughingStock.length-1].getIsGrassFed() + " " + laughingStock[laughingStock.length-1].getLiveOutdoor());
		System.out.println("This is the no.4 cattle. Type <male> or <female> to determine its gender.");
		gender = reader.next();
		laughingStock[laughingStock.length-1].setGender(gender);
		System.out.println(laughingStock[laughingStock.length-1].getGender() + " " + laughingStock[laughingStock.length-1].getIsGrassFed() + " " + laughingStock[laughingStock.length-1].getLiveOutdoor());
	}
}