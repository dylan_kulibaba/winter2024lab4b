public class Cattle{
	//Attributes
	private String gender;
	private boolean isGrassFed;
	private boolean liveOutdoor;
	
	//GETTERS
	public String getGender(){
		return this.gender;
	}

	public boolean getIsGrassFed(){
		return isGrassFed;
	}

	public boolean getLiveOutdoor(){
		return liveOutdoor;
	}

	//SETTERS
	public void setGender(String gender){
		this.gender = gender;
	}

	//Constructor
	public Cattle(String g, boolean isGrassFed, boolean liveOutdoor){
		this.gender = g;
		this.isGrassFed = isGrassFed;
		this.liveOutdoor = liveOutdoor;
	}
	
	//Action 1
	public void revealGender(){
		if (this.gender.equals("male"))
			System.out.println("This is a male cattle, people call it a bull. Bulls love you as much as you love your steak!");
		else if (this.gender.equals("female"))
			System.out.println("This is a female cattle, people call it a cow. The cow is mooing to communicate to you!");
		else 
			System.out.println("You put a wrong gender to an animal, it should only be either male or female");
	}
	
	//Action2
	public void isHappy(){
		if (liveOutdoor)
			System.out.println("Moo, I'm a cattle who usually hangs out with friends on pasture and eat grasses. I'm a happy animal!");
		else 
			System.out.println("Moo, I'm a cattle who barely live outside of my barn. I missed the sun! I'm not a happy animal!");
	}
}